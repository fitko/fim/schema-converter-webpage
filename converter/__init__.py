import os
from flask import Flask, request, send_file, abort
import tempfile
from io import BytesIO
import pathlib
import subprocess


PACKAGE_DIR = pathlib.Path(__file__).parent
CONVERTER = PACKAGE_DIR / "fim-schema-converter.jar"

with open(PACKAGE_DIR / "page.html", encoding="utf-8") as file:
    HTML = file.read()

with open(PACKAGE_DIR / "style.css", encoding="utf-8") as file:
    CSS = file.read().encode("utf-8")


class ConversionError(Exception):
    pass


def convert_xml(content: str, code_lists: dict[str, str]) -> str:
    with tempfile.TemporaryDirectory() as dir:
        tmp_dir = pathlib.Path(dir)
        schema_file = tmp_dir / "schema.xml"

        # write schema.xml
        with open(schema_file, "w", encoding="utf-8") as file:
            file.write(content)

        # write code lists
        for name, list_content in code_lists.items():
            filename = os.path.basename(name)
            with open(tmp_dir / filename, "w", encoding="utf-8") as file:
                file.write(list_content)

        result = subprocess.run(
            ["java", "-jar", CONVERTER, "-i", schema_file],
            capture_output=True,
        )

        if result.returncode == 0:
            return result.stdout.decode("utf-")
        else:
            raise ConversionError()


def create_app():
    app = Flask(__name__)

    @app.route("/style.css")
    def style():
        return send_file(BytesIO(CSS), mimetype="text/css")

    @app.route("/")
    def hello_world():
        return HTML

    @app.route("/convert", methods=["POST"])
    def convert():
        data = request.files.get("data")
        if data is None:
            abort(422)

        lists = request.files.getlist("lists")
        code_lists = {
            l.filename: l.stream.read().decode("utf-8")
            for l in lists
            if l.filename != ""
        }
        print(code_lists)

        content = data.stream.read().decode("utf-8")
        schema = convert_xml(content, code_lists)

        filename = data.filename
        if filename is not None:
            basename = os.path.splitext(filename)[0]
            download_name = f"{basename}.schema.json"
        else:
            download_name = "unknown.schema.json"

        io = BytesIO(schema.encode("utf-8"))

        return send_file(
            io, as_attachment=True, mimetype="text/json", download_name=download_name
        )

    return app

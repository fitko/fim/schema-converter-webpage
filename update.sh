#!/usr/bin/env bash
set -exo pipefail
# don't use 'set -euxo pipefail' as the command 'deactivate' of venv produces an 'unbound variable' error.

cd ~/schema-converter

source ./env3_10/bin/activate
pip uninstall -y converter
pip install ./converter-0.1.0-py3-none-any.whl
deactivate

supervisorctl restart schema-converter

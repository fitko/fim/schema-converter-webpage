#!/usr/bin/env bash
set -euxo pipefail

poetry build
scp ./dist/converter-0.1.0-py3-none-any.whl fitko:~/schema-converter
scp ./update.sh fitko:~/schema-converter

ssh fitko 'bash -s' < update.sh

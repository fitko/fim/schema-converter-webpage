#!/usr/bin/env bash
set -euxo pipefail

cd ./fim-schema-converter
mvn package
cp ./target/fim-schema-converter.jar ../converter/
